<?php

session_start();

class SimpleLogin {
    public function login($email, $password) {
        $newLogin = (object) [
            'email' => $email,
            'password' => $password
        ];

        if ($_SESSION['users'] === null) {
            $_SESSION['users'] = array();
        }

        array_push($_SESSION['users'], $newLogin);
    }

    public function clear() {
        session_destroy();
    }
}

$userLogin = new SimpleLogin();

if ($_POST['action'] === 'login') {
    $userLogin->login($_POST['email'], $_POST['password']);
} else if ($_POST['action'] === 'clear') {
    $userLogin->clear();
}

header('Location: ./index.php');

?>
